== Lista de asistentes

// Ordenados por orden alfabético
// Formato: Apellidos, Nombre

* Aguilar Hernández, Echedey
* Cebrero Ávila, Antonio Manuel
* Caño Soto, Álvaro
* Cuevas Fernández, Alejandro
* Durán Nieto, Julián
* García-Amorena Reina, Julio
* García Arroyo, Manuel
* González Lozano, Luis
* Labandon Mateos, Antonio 
* Leon Andrades, Jose Maria
* Luque Giráldez, José Rafael
* Madroñal Sánchez, Óscar
* Miranda Alonso, Victor
* Rodríguez García, Marcos A.
* Rodríguez Salcedo, Ángel
* Romero Cruz, Álvaro

=== Organización del transporte

// Si tienes vehículo, pon el número de plazas. Si no tienes, añádete a
// alguno de los vehículos existentes.

==== Dacia Duster (5 plazas)

* Alejandro Cuevas
* Julián Durán
* Antonio Labandon
* Ángel Rodríguez
* Álvaro Caño

==== Ford Mondeo (5 plazas)

* Manuel García
* Julio García-Amorena

==== Volkswagen Golf (5 plazas)

* Antonio Manuel Cebrero
* Luis González Lozano
* Jose Maria Leon

==== Peugeot 508 (5 plazas)

* Echedey Aguilar
* Rafael Luque
* Álvaro Romero
* Óscar Madroñal
* Marcos Rodríguez

==== Toyota Corola Verso (7 plazas)

* Víctor Miranda